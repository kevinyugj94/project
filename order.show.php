<?php
session_start();
require("orderModel.php");
//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Orders page</title>
</head>
<body>
<p>This is the list of you historical orders</p>
<hr>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<hr>";
?>
	<table width="400" border="1">
  <tr>
    <td>order ID</td>
    <td>Date</td>
    <td>Status</td>
	<td> detail </td>
  </tr>

<?php
$result=getOrderList($_SESSION["loginProfile"]["uID"]);
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['ordID'] . "</td>";
	echo "<td>{$rs['orderDate']}</td>";
	echo "<td align = right>" , $rs['status'], "</td>";
	echo "<td align = right><a href='order.showDetail.php?ID=" , $rs['ordID'] , "'>ShowDetail</a></td></tr>";
	//echo "<td><a href='addToCart.php?prdID='" . $rs['prdID'] . "'>Add</a></td>";
	echo "</tr>";
}
?>
</table>
<hr>
<a href="main.php">OK</a>

</body>
</html>
