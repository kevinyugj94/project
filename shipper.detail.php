<?php
session_start();
require("orderModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}

$ordID=(int)$_GET['ID'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ShipperDetail</title>
</head>
<body>
<p>Your Order detail is : 
[<a href="logout.php">logout</a>]
</p>
<hr>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
	$result = getOrderDetail($ordID);
	$address = getOrderAddress($ordID);

?>

	<table width="300" border="1">
  <tr>
  <td colspan = 5 align = center>OrdID</td></tr>
  <?php
  echo "<tr><td colspan = 5 align = center>" .$ordID. "</td></tr>";
  ?>
  <td colspan = 5 align = center>Address</td></tr>
  <?php
  echo "<tr><td colspan = 5 align = center>" .$address. "</td></tr>";
  ?>


  <tr>
    <td>Prd Name</td>
    <td>price</td>
    <td>Quantity</td>
    <td>Amount</td>
  </tr>
<?php
$total=0;
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<td>{$rs['name']}</td>";
	echo "<td>" , $rs['price'], "</td>";
	echo "<td align = center>" , $rs['quantity'], "</td>";
	$total += $rs['quantity'] *$rs['price'];
	echo "<td align = right>" , $rs['quantity'] *$rs['price'] , "</td>";
	echo "</tr>";
}
echo "<tr><td colspan = 5 align = right>Total: $total</td></tr>";
?>
</table>

<?php
    if($_SESSION["loginProfile"]["uRole"] == 6) {
		echo "<a href='order.delivered.php?id=",$ordID,"'>OK, 已送達目的地</a>";
        echo "<hr>";
        echo "<a href='shipper.php'>back</a>";
	}
	if($_SESSION["loginProfile"]["uRole"] == 1) {
        echo "<a href='order.show.php'>back</a>";
	}
?>
</form>
</body>
</html>