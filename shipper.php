<?php
require("utils.php");
chkAccess(6,"main.php");
require("orderModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
if ($_SESSION['loginProfile']['uRole'] != 6) {
	header("Location: main.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shipping page</title>
</head>
<body>
<p>This is the Shipping page 
[<a href="logout.php">logout</a>]
[<a href = "prdMain.php">Go to Product Main page</a>]
</p>
<hr>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
?>
<table width="400" border="1">
  <tr>
    <td align = center>ordID</td>
    <td align = center>name</td>
    <td align = center>order date</td>
    <td align = center>Address</td>
  </tr>


<?php
$result=getConfirmedOrderList2();
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td align = center>" . $rs['ordID'] . "</td>";
	echo "<td align = center>{$rs['uID']}</td>";
    echo "<td align = center>" , $rs['orderDate'], "</td>";
    echo "<td align = center>" , $rs['address'], "</td>";
	echo "<td align = right><a href='shipper.detail.php?ID=" , $rs['ordID'] , "'>ShowDetail</a></td></tr>";
}
?>

</table>
</body>
</html>
